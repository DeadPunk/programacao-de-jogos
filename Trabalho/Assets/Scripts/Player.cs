﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public int objects = 0;
	public Transform waterTransform;
    public Rigidbody rb;
    public int jump;

    public Transform cam;

    public float diveForce;


    // Start is called before the first frame update
    void Start()
    {
        diveForce = 1;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
   void Update()
    {
    	if(transform.position.y < waterTransform.position.y-0.6f){
            RenderSettings.fog = true;
        }   		
    	else{
            RenderSettings.fog = false;
        }

        if(transform.position.y < waterTransform.position.y){
            rb.velocity = (transform.up) * -0.3f;
            diveForce = cam.rotation.x > 0 ? 0.8f : -0.5f;
            if(jump>0){
                rb.velocity = (transform.forward*0.75f)+(transform.up*(diveForce));
                jump--;
            }
            if(Input.GetKey("space")){
                jump=10;
            }
        }
    		
        
    }

    void OnCollisionEnter(){

    }
}
